package com.gl.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gl.model.Student;
import com.gl.repository.StudentRepository;

@Service
public class StudentService {

	private StudentRepository studentRepository;
	
	public StudentService(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}
	
	public Student saveStudent(Student student) {
		return this.studentRepository.save(student);
	}
	
	public List<Student> getAllStudents() {
		return this.studentRepository.findAll();
	}
	
	public Student fetchStudent(long id) {
		
		return null;
	}
	
	public void deleteStudentById(long id) {
		
	}
	
}
