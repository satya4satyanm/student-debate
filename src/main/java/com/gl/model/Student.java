package com.gl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="students")
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "student_id")
	private long studentId;
	
	@Column(name = "student_name", nullable=false)
	private String studentName;
	
	@Column(name = "student_dept", nullable = false)
	private String studentDept;
	
	@Column(name = "student_country", nullable=false)
	private String stuentCountry;
	
	public Student() {}

	public Student(String studentName, String studentDept, String stuentCountry) {
		this.studentName = studentName;
		this.studentDept = studentDept;
		this.stuentCountry = stuentCountry;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentDept() {
		return studentDept;
	}

	public void setStudentDept(String studentDept) {
		this.studentDept = studentDept;
	}

	public String getStuentCountry() {
		return stuentCountry;
	}

	public void setStuentCountry(String stuentCountry) {
		this.stuentCountry = stuentCountry;
	}
	
	
}
