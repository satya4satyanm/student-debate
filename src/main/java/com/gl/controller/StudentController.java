package com.gl.controller;

import java.util.List;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gl.model.Student;
import com.gl.service.StudentService;

@RestController
@RequestMapping("/students/")
public class StudentController {
	
	private final StudentService studentService;
	
	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}
	
	@PostMapping("/save")
	public String saveStudent(Model model) {
		Student student = new Student();
		model.addAttribute(student);
		return "save-student";
	}
	
	@GetMapping("/all")
	public String getAllStudents(Model model) {
		List<Student> students = studentService.getAllStudents();
		model.addAttribute(students);
		return "list-student";
	}
	
	@RequestMapping("/get/id")
	public String fetchStudentById(long id) {
		return "";
	}
	
	@RequestMapping("/delete/id")
	public void deleteStudentById(@RequestParam("studentId") int studentId) {
		studentService.deleteStudentById(studentId);
	}

}
